import Vue from 'vue'
import Router from 'vue-router'
import Session from './views/Session.vue'
import TutorSignup from './views/TutorSignup.vue'
import Classumo from './views/Classumo.vue'

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/session',
			component: Session,
		},
		{
			path: '/tutor-signup',
			component: TutorSignup,
		},		
		{
			path: '/student-signup',
			component: Classumo,
		}
	]
})
