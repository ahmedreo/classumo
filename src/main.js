import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins'
import './directives'
import 'typeface-roboto'
import './assets/sass/main.scss';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),

  // Global MQ values.
  mq: {
    phone: '(max-width: 768px)',
    tablet: '(max-width: 1025px)',
    desktop: '(min-width: 1025px)'
  },
}).$mount('#app')
