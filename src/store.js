import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		messanger: true
	},
	mutations: {
		toggleMessanger(state){
			state.messanger = ! state.messanger
		}	
	}
})
