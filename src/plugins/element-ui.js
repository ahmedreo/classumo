import Vue from 'vue';

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import Slider from 'element-ui/lib/slider';
import Input from 'element-ui/lib/input';
import Button from 'element-ui/lib/button';
import Select from 'element-ui/lib/select';
import Upload from 'element-ui/lib/upload';
import InputNumber from 'element-ui/lib/input-number';
import Dialog from 'element-ui/lib/dialog';
import Form from 'element-ui/lib/form';
import FormItem from 'element-ui/lib/form-item';
import Col from 'element-ui/lib/col';
import Row from 'element-ui/lib/row';
import Option from 'element-ui/lib/option';
import DatePicker from 'element-ui/lib/date-picker';
import Dropdown from 'element-ui/lib/dropdown';
import DropdownMenu from 'element-ui/lib/dropdown-menu';
import DropdownItem from 'element-ui/lib/dropdown-item';
import Header from 'element-ui/lib/header';
import Main from 'element-ui/lib/main';
import Aside from 'element-ui/lib/aside';
import Container from 'element-ui/lib/container';
import Steps from 'element-ui/lib/steps';
import Step from 'element-ui/lib/step';

import 'element-ui/lib/theme-chalk/index.css';

Vue.use(Slider);
Vue.use(Input);
Vue.use(Button);
Vue.use(Select);
Vue.use(Upload);
Vue.use(InputNumber);
Vue.use(Dialog);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Col);
Vue.use(Row);
Vue.use(Option);
Vue.use(DatePicker);
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Header);
Vue.use(Main);
Vue.use(Aside);
Vue.use(Container);
Vue.use(Steps);
Vue.use(Step);

locale.use(lang);