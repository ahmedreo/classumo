import Vue from 'vue'
import positions from 'positions'

Vue.directive('offset', {
	inserted(el){
		let target = el.parentElement
		let css = positions(el , 'center left' , target , 'center right')
		let height = el.offsetHeight 

		el.style.top = css.top + 'px'
		el.style.left = css.left + 12 + 'px'
		
		if (el.offsetTop + height > window.innerHeight) {
			el.style.bottom = 0 + 'px'
			el.style.top = 'unset'
		}
		if (el.offsetTop < 0) {
			el.style.top = 0 + 'px'
			el.style.bottom = 'unset'
		}
	}
})