import Vue from 'vue'
import * as vClickOutside from 'v-click-outside-x'
Vue.directive('click-outside', vClickOutside.directive)