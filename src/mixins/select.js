export default {
	data: () => ({
		selected: '',
		extra : {}
	}),
	methods: {
		is(prop){
			if (prop.indexOf('.') !== -1) {
				let arr = prop.split('.')
				return (this.extra[arr[0]] === arr[1]) && 'selected'
			}
			else {
				return (this.selected === prop) && 'selected'
			}
		},
		select(item){
			if (item.indexOf('.') !== -1) {
				let arr = item.split('.')
				this.$set(this.extra , arr[0] , arr[1])
			}
			else {
				this.selected = item
			}
		}
	}
}