export default {
	data: () => ({
		dropdown: ''
	}),
	methods: {
		isDropdown(dropdown){
			return this.dropdown === dropdown
		},
		toggleDropdown(dropdown){
			this.dropdown !== dropdown && this.$nextTick(() => { this.dropdown = dropdown })
		},
		closeDropdown(){
			this.dropdown = ''
		},
		
	}
}